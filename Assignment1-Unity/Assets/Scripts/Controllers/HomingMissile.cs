﻿using UnityEngine;
using System.Collections;

public class HomingMissile : MonoBehaviour
{
    public float ForwardSpeed = 1;
    public float RotateSpeedInDeg = 45;

    // In Update, you should rotate and move the missile to rotate it towards the player.  It should move forward with ForwardSpeed and rotate at RotateSpeedInDeg.
    // Do not use the RotateTowards or LookAt methods.
    void Update()
    {
        transform.Translate(transform.up * ForwardSpeed*Time.deltaTime, Space.World);

        Vector3 vecToEnemy = GameController.GetEnemyObject().transform.position - transform.position;

        float crossZ = (transform.up.x * vecToEnemy.y - transform.up.y * vecToEnemy.x);

        transform.Rotate(0, 0, crossZ > 0 ? 1 : -1 * RotateSpeedInDeg * Time.deltaTime);
    }
}
