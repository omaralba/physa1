﻿using UnityEngine;
using System.Collections;

public class VisionCone : MonoBehaviour
{

    public float AngleSweepInDegrees = 60;
    public float ViewDistance = 3;

    /// <summary>
    /// Calculates whether the player is inside the vision cone of an enemy as defined by the AngleSweepIndegrees
    /// and ViewDistance varilables. Do not use any magnitude or Distance methods.  You may use any of the previous
    /// methods you have written.
    /// </summary>
    /// <see cref="GameController"/>
    /// <returns>Whether the player is within the enemy's vision cone.</returns>
    public bool IsPlayerInVisionCone()
    {

        Vector3 vectorToPlayer = GameController.GetPlayerObject().transform.position - transform.position;
        float distanceToPlayer = Mathf.Sqrt(Mathf.Pow(vectorToPlayer.x, 2) + Mathf.Pow(vectorToPlayer.y, 2));
        float angleToPlayer = Mathf.Atan2(vectorToPlayer.x, vectorToPlayer.y) * Mathf.Rad2Deg;
        float enemyUpAngle = Mathf.Atan2(transform.up.x, transform.up.y) * Mathf.Rad2Deg;

        if(angleToPlayer <= enemyUpAngle + (AngleSweepInDegrees / 2) && angleToPlayer >= enemyUpAngle - (AngleSweepInDegrees/2) && distanceToPlayer <= ViewDistance)
        {
            return true;
        }

        else
        {
            return false;
        }

      
    }
    
}
