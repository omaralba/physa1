﻿using UnityEngine;
using System.Collections;

public class BombLine : MonoBehaviour
{

    public GameObject BombPrefab;
    public int BombCount;
    public float BombSpacing;

    /// <summary>
    /// Spawn a line of instantiated BombPrefabs behind the player ship. There should be BombCount bombs placed with BombSpacing amount of space between them.
    /// </summary>
    /// <returns>An array containing all the bomb objects</returns>
    public GameObject[] SpawnBombs()
    {
        GameObject[] bombButLineShaped = new GameObject[BombCount];
        for(int i = 0; i<BombCount; i++)
        {
            GameObject newBomb = GameObject.Instantiate(BombPrefab, (transform.up)*-1*(BombSpacing*(i+1)) + new Vector3(transform.position.x, transform.position.y ),Quaternion.identity);
            bombButLineShaped[i] = newBomb;
        }

        

        return bombButLineShaped;
    }
    
}
