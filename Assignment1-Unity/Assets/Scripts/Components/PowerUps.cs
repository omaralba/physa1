﻿using UnityEngine;
using System.Collections;

public class PowerUps : MonoBehaviour
{
    public GameObject PowerUpPrefab;
    public int PowerUpCount = 3;
    public float PowerUpRadius = 1;

    /// <summary>
    /// Spawn a circle of PowerUpCount power up prefabs stored in PowerUpPrefab, evenly spaced, around the player with a radius of PowerUpRadius
    /// </summary>
    /// <returns>An array of the spawned power ups, in counter clockwise order.</returns>
    /// a
    public GameObject[] SpawnPowerUps()
    {
        GameObject[] powerItUp = new GameObject[PowerUpCount];
        for (int i = 0; i < PowerUpCount; i++)
        {
            float angelaMerkle = Mathf.PI * 2 / PowerUpCount*(i+1);
            Vector3 powerUpPosition = new Vector3(Mathf.Cos(angelaMerkle), Mathf.Sin(angelaMerkle), 0) * PowerUpRadius;
            powerUpPosition = powerUpPosition + transform.position;

            GameObject unlimitedPower = Instantiate(PowerUpPrefab, powerUpPosition, Quaternion.identity);
        }
            return powerItUp;
    }
}
