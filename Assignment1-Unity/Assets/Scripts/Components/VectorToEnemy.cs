﻿using UnityEngine;
using System.Collections;

public class VectorToEnemy : MonoBehaviour
{

    /// <summary>
    /// Calculated vector from the player to enemy found by GameManager.GetEnemyObject
    /// </summary>
    /// <see cref="GameController.GetEnemyObject"/>
    /// <returns>The vector from the player to the enemy.</returns>
    public Vector3 GetVectorToEnemy()
    {
        Debug.DrawLine(transform.position, GameController.GetEnemyObject().transform.position, Color.green);
        Vector3 distanceFromEnemy = GameController.GetEnemyObject().transform.position - transform.position;
        return distanceFromEnemy;
    }

    /// <summary>
    /// Calculates the distance from the player to the enemy returned by GameManager.GetEnemyObject without using calls to magnitude.
    /// </summary>
    /// <see cref="GameController.GetEnemyObject"/>
    /// <returns>The scalar distance between the player and the enemy</returns>
    public float GetDistanceToEnemy()
    {
        float distanceToEnemy = Mathf.Sqrt(Mathf.Pow(GetVectorToEnemy().x, 2) + Mathf.Pow(GetVectorToEnemy().y, 2));
        return distanceToEnemy;
    }
    
}
